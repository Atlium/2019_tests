package io.humb1t.calculator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CalculatorTestNegative {

    private Calculator calculator;

    @Before
    public void init() {
        calculator = new Calculator();
    }

    @Test(expected = ArithmeticException.class)
    public void division() {
        calculator.division(2, 0);
    }

    @Test(expected = ArithmeticException.class)
    public void sqrt() {
        calculator.sqrt(-1);
    }

    @After
    public void destroy() {
        calculator = null;
    }

}