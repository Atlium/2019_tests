package io.humb1t.calculator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(NumberRowSupplier.class)
public class CalculatorTestPositive {

    private static final double DELTA = 1e-15;
    private NumberRowSupplier numberRowSupplier;
    private Calculator calculator;

    @Before
    public void init() {
        numberRowSupplier = new NumberRowSupplier();
        calculator = new Calculator();
    }

    @After
    public void destroy() {
        numberRowSupplier = null;
        calculator = null;
    }

    @Test
    public void sum() {
        int expected = 14;
        assertEquals(expected, calculator.sum(10, 4));
    }

    @Test
    public void sumWithDelta() {
        double expected = 12;
        assertEquals(expected, calculator.sum(9d, 3d), DELTA);
    }

    @Test
    public void subtraction() {
        int expected = 6;
        assertEquals(expected, calculator.subtraction(10, 4));
    }

    @Test
    public void subtractionWithDelta() {
        double expected = 6;
        assertEquals(expected, calculator.subtraction(9d, 3d), DELTA);
    }

    @Test
    public void multiplication() {
        int expected = 40;
        assertEquals(expected, calculator.multiplication(10, 4));
    }

    @Test
    public void multiplicationWithDelta() {
        double expected = 27;
        assertEquals(expected, calculator.multiplication(9d, 3d), DELTA);
    }

    @Test
    public void division() {
        double expected = 2.5;
        assertEquals(expected, calculator.division(10, 4), DELTA);
    }

    @Test
    public void divisionWithDelta() {
        double expected = 3;
        assertEquals(expected, calculator.division(9d, 3d), DELTA);
    }

    @Test
    public void sqrt() {
        double expected = 3;
        assertEquals(expected, calculator.sqrt(9d), DELTA);
    }

    @Test(timeout = 10)
    public void isPrimeNegative() {
        int i = -8;
        assertTrue(!calculator.isPrime(i));
    }

    @Test(timeout = 10)
    public void isPrimePositive() {
        int i = 7;
        assertTrue(calculator.isPrime(i));
    }

    @Test
    public void fibIndexThree() {
        int index = 3;
        int expected = 2;
        assertEquals(expected, calculator.fib(index));
    }

    @Test
    public void fibIndexSix() {
        int index = 6;
        int expected = 8;
        assertEquals(expected, calculator.fib(index));
    }

    //public
    @Test
    public void summarizeNumberRowPublic() {
        int[] arr = numberRowSupplier.getPredefinedPublic();
        assertEquals(12, calculator.summarizeNumberRow(arr));
    }

    @Test
    public void summarizeNumberRowPublicMock() {
        NumberRowSupplier rowSupplier = mock(NumberRowSupplier.class);
        when(rowSupplier.getPredefinedPublic()).thenReturn(new int[]{1, 1, 1});
        int[] arr = rowSupplier.getPredefinedPublic();
        assertEquals(3, calculator.summarizeNumberRow(arr));
    }

    @Test
    public void summarizeNumberRowPublicSpy() {
        NumberRowSupplier spy = spy(numberRowSupplier);
        when(spy.getPredefinedPublic()).thenReturn(new int[]{3, 3, 3});
        int[] arr = spy.getPredefinedPublic();
        assertEquals(9, calculator.summarizeNumberRow(arr));
    }

    //private
    @Test
    public void summarizeNumberRowPrivateReflection() throws Exception {
        Class<?> supplierClass = numberRowSupplier.getClass();
        Method method = supplierClass.getDeclaredMethod("getPredefinedPrivate");
        method.setAccessible(true);
        int[] arr = (int[]) method.invoke(supplierClass.newInstance());
        assertEquals(24, calculator.summarizeNumberRow(arr));
        method.setAccessible(false);

    }

    @Test
    public void summarizeNumberRowPrivatePowerMock() throws Exception {
        NumberRowSupplier powerMock = PowerMockito.spy(numberRowSupplier);
        PowerMockito.doReturn(new int[]{5, 5, 5}).when(powerMock, "getPredefinedPrivate");
        int[] arr = powerMock.getPredefinedFromPrivate();
        assertEquals(15, calculator.summarizeNumberRow(arr));
    }

    //static
    @Test
    public void summarizeNumberRowStatic() {
        int[] arr = NumberRowSupplier.getPredefinedStatic();
        assertEquals(15, calculator.summarizeNumberRow(arr));
    }

    @Test
    public void summarizeNumberRowStaticPowerMock() {
        PowerMockito.mockStatic(NumberRowSupplier.class);
        when(NumberRowSupplier.getPredefinedStatic()).thenReturn(new int[]{1, 1, 1});
        int[] arr = NumberRowSupplier.getPredefinedStatic();
        assertEquals(3, calculator.summarizeNumberRow(arr));

    }

    //final
    @Test
    public void summarizeNumberRowFinal() {
        int[] arr = numberRowSupplier.getPredefinedFinal();
        assertEquals(6, calculator.summarizeNumberRow(arr));
    }

    @Test
    public void summarizeNumberFinalPowerMock() throws Exception {
        NumberRowSupplier powerMock = PowerMockito.mock(NumberRowSupplier.class);
        PowerMockito.doReturn(new int[]{5, 5, 5}).when(powerMock, "getPredefinedFinal");
        int[] arr = powerMock.getPredefinedFinal();
        assertEquals(15, calculator.summarizeNumberRow(arr));
    }
}