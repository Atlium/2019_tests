package io.humb1t;

import io.humb1t.calculator.CalculatorTestNegative;
import io.humb1t.calculator.CalculatorTestPositive;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({CalculatorTestNegative.class, CalculatorTestPositive.class})
public class MyTestSuite {
}
