package io.humb1t;

import org.apache.log4j.Logger;

public class Main {
    private static final Logger ROOT_LOGGER = Logger.getRootLogger();
    private static final Logger MAIN_LOGGER = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        MAIN_LOGGER.info("MAIN");
        ROOT_LOGGER.info("ROOT");
    }
}
