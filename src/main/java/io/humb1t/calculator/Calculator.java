package io.humb1t.calculator;

import org.apache.log4j.Logger;

import java.util.Arrays;

public class Calculator {

    private static final Logger LOGGER = Logger.getLogger(Calculator.class);

    public int sum(int a, int b) {
        LOGGER.info("sum int, values: " + a + " " + b);
        return a + b;
    }

    public double sum(double a, double b) {
        LOGGER.info("sum double, values: " + a + " " + b);
        return a + b;
    }

    public int subtraction(int a, int b) {
        LOGGER.info("subtraction int, values: " + a + " " + b);
        return a - b;
    }

    public double subtraction(double a, double b) {
        LOGGER.info("subtraction double, values: " + a + " " + b);
        return a - b;
    }

    public int multiplication(int a, int b) {
        LOGGER.info("multiplication int, values: " + a + " " + b);
        return a * b;
    }

    public double multiplication(double a, double b) {
        LOGGER.info("multiplication double, values: " + a + " " + b);
        return a * b;
    }

    public double division(double a, double b) {
        if (b == 0) {
            LOGGER.error("### ERROR ### division, values: " + a + " " + b);
            throw new ArithmeticException("divide in zero, b = 0 !!!");
        }
        LOGGER.warn("### WARN ### division, values: " + a + " " + b);
        return a / b;
    }


    public double sqrt(double a) {
        if (a < 0) {
            LOGGER.error("### ERROR ### sqrt, values: " + a);
            throw new ArithmeticException("sqrt from negative number, a = " + a);
        }
        LOGGER.warn("### WARN ### sqrt, values: " + a);
        return Math.sqrt(a);
    }

    public boolean isPrime(int value) {
        LOGGER.info("isPrime, values: " + value);
        if (value < 1) {
            return false;
        }
        int temp;
        for (int i = 2; i <= value / 2; i++) {
            temp = value % i;
            if (temp == 0) {
                return false;
            }
        }
        return true;
    }

    public int fib(int a) {
        LOGGER.info("fib, values: " + a);
        if (a == 1) return 1;
        if (a == 2) return 1;
        return fib(a - 1) + fib(a - 2);
    }

    public int summarizeNumberRow(int[] row) {
        LOGGER.info("summarizeNumberRow, values: " + Arrays.toString(row));
        int result = 0;
        for (int i : row) {
            result += i;
        }
        return result;
    }
}
