package io.humb1t.calculator;

public class NumberRowSupplier {
    final int[] getPredefinedFinal() {
        return new int[]{1, 2, 3};
    }

    static int[] getPredefinedStatic() {
        return new int[]{4, 5, 6};
    }

    private int[] getPredefinedPrivate() {
        return new int[]{7, 8, 9};
    }

    public int[] getPredefinedPublic() {
        return new int[]{2, 4, 6};
    }

    public int[] getPredefinedFromPrivate() {
        return getPredefinedPrivate();
    }
}
